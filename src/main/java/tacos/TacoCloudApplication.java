package tacos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"tacos.repository", "tacos.config", "tacos.controller", "tacos.security"})
public class TacoCloudApplication {


	public static void main(String[] args) throws Exception {
		SpringApplication.run(TacoCloudApplication.class, args);

	}
}
