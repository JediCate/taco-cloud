package tacos.controller;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import tacos.domain.TacoOrder;
import tacos.domain.User;
import tacos.repository.OrderRepositoryCustom;
import tacos.repository.UserRepository;

@Slf4j
@Controller
@RequestMapping("/orders")
@SessionAttributes("tacoOrder")
public class OrderController {
    private final UserRepository userRepository;

    private final static Logger logger = LoggerFactory.getLogger(OrderController.class);

    private OrderRepositoryCustom<TacoOrder> orderRepoCustom;

    @Autowired
    public OrderController(OrderRepositoryCustom<TacoOrder> orderRepoCustom,
                           UserRepository userRepository){
        this.orderRepoCustom = orderRepoCustom;
        this.userRepository = userRepository;
    }

    @GetMapping("/current")
    public String orderForm(){
        return "orderForm";
    }

    @PostMapping
    public String processOrder(@Valid TacoOrder order, Errors errors, SessionStatus sessionStatus,
                               @AuthenticationPrincipal User user){
        if(errors.hasErrors()){
            System.out.println(errors);
            return "orderForm";
    }
        log.info("Order submitted: {}", order);
        //User user = userRepository.findByUsername(principal.getName());
        //User user = (User) auth.getPrincipal();
        order.setTacoUser(user);
        orderRepoCustom.save(order);

        sessionStatus.setComplete();

        return "redirect:/";
    }
}
