package tacos.security;

import lombok.Data;
import org.springframework.security.crypto.password.PasswordEncoder;
import tacos.domain.User;

@Data
public class RegistrationForm {

    private String username;
    private String password;
    private String fullname;
    private String street;
    private String city;

    private String state;
    private String zip;
    private String phone;

    public User toUser(PasswordEncoder passwordEncoder){
        User user = new User(username, passwordEncoder.encode(password), fullname, street, city, state, zip, phone);
        System.out.println("length: " + passwordEncoder.encode(password).length() + "  " + passwordEncoder.encode(password));
        System.out.println(user);
        return user;
    }
}
