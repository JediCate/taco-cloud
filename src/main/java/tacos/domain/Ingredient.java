package tacos.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

@Data
@Entity
@Table(name="ingredient", schema="public")
@RequiredArgsConstructor
@AllArgsConstructor
//@NoArgsConstructor(access= AccessLevel.PRIVATE, force = true)
public class Ingredient implements Serializable {

    @Id
    private String id;
    private String name;
    @Column(name = "type", columnDefinition = "varchar(10) not null")
    @Enumerated(EnumType.STRING)
    private Type type;

    public enum Type {
        WRAP, PROTEIN, VEGGIES, CHEESE, SAUCE
    }
}
