package tacos.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@Table(name="taco", schema="public")
public class Taco implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDateTime createdAt=LocalDateTime.now();
    @NotNull
    @Size(min=5, message="Name must be at least 5 ch long")
    private String name;
    @NotNull
    @Size(min=1, message = "You must choose at least one ingredient")
    @ManyToMany(cascade = { CascadeType.ALL })
    private List<IngredientRef> ingredients;
}
