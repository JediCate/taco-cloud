package tacos.domain;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serializable;

@Data
@Entity
@Table(name="ingredient_ref", schema="public")
@NoArgsConstructor
@AllArgsConstructor
public class IngredientRef implements Serializable {
   @Id
   private String ingredient;
}
