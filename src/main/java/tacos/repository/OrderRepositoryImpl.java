package tacos.repository;

import org.springframework.asm.Type;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import tacos.domain.IngredientRef;
import tacos.domain.Taco;
import tacos.domain.TacoOrder;

import java.sql.Types;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
@Primary
@Repository
public class OrderRepositoryImpl implements OrderRepositoryCustom<TacoOrder>{

    private JdbcOperations jdbcOperations;
    private UserRepository userRepo;

    public OrderRepositoryImpl(JdbcOperations jdbcOperations, UserRepository userRepo){
        this.jdbcOperations = jdbcOperations;
        this.userRepo = userRepo;
    }

    @Override
    public TacoOrder save(TacoOrder order) {
        System.out.println("ENTERING SAVE METHOD");
        PreparedStatementCreatorFactory stmt = new PreparedStatementCreatorFactory(
                """
        insert into taco_order (delivery_name, delivery_street, delivery_city, delivery_state, delivery_zip,
        cc_number, cc_expiration, cc_cvv, placed_at, taco_user) values(?,?,?,?,?,?,?,?,?,?)""",
                Types.VARCHAR, Types.VARCHAR,Types.VARCHAR,
                Types.VARCHAR,Types.VARCHAR,Types.VARCHAR,
                Types.VARCHAR,Types.VARCHAR, Types.TIMESTAMP, Types.BIGINT);
        //we need to get the id of the saved order
        stmt.setReturnGeneratedKeys(true);

        order.setPlacedAt(LocalDateTime.now());

        PreparedStatementCreator stmtCreator = stmt.newPreparedStatementCreator(
                Arrays.asList(order.getDeliveryName(),
                        order.getDeliveryStreet(),
                        order.getDeliveryCity(),
                        order.getDeliveryState(),
                        order.getDeliveryZip(),
                        order.getCcNumber(),
                        order.getCcExpiration(),
                        order.getCcCVV(),
                        order.getPlacedAt(),
                        order.getTacoUser().getId()));

        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcOperations.update(stmtCreator, keyHolder);
        long orderId = Long.valueOf((int)keyHolder.getKeyList().get(0).get("id"));
        order.setId(orderId);

        List<Taco> tacos = order.getTacos();
        int i = 0;
        for (Taco taco: tacos) {
            saveTaco(orderId, i++, taco);
        }
        return null;
    }

    private long saveTaco(Long orderId, int orderKey, Taco taco){
        taco.setCreatedAt(LocalDateTime.now());

        PreparedStatementCreatorFactory stmt = new PreparedStatementCreatorFactory(
                """
                        insert into taco(name, created_at, taco_order, taco_order_key) 
                        values(?,?,?,?)""",
                Types.VARCHAR, Types.TIMESTAMP, Type.LONG, Type.LONG
        );
        stmt.setReturnGeneratedKeys(true);

        PreparedStatementCreator stmtCreator = stmt.newPreparedStatementCreator(
                Arrays.asList(taco.getName(),
                        taco.getCreatedAt(),
                        orderId,
                        orderKey)
        );

        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcOperations.update(stmtCreator, keyHolder);
        long tacoId = Long.valueOf((int)keyHolder.getKeyList().get(0).get("id"));
        taco.setId(tacoId);

        saveIngredientReft(tacoId, taco.getIngredients());

        return tacoId;
    }

    private void saveIngredientReft(long tacoId, List<IngredientRef> ingredientRefs ){
        int key = 0;
        for (IngredientRef ref : ingredientRefs) {
            jdbcOperations.update(
                    """
                            insert into ingredient_ref(ingredient, taco, taco_key) 
                            values(?,?,?)""", ref.getIngredient(), tacoId, key++
            );
        }
    }

}
