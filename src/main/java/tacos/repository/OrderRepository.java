package tacos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tacos.domain.TacoOrder;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface OrderRepository extends OrderRepositoryCustom<TacoOrder>, JpaRepository<TacoOrder, Long> {

    /**
     * in order to override an implementation from JpaRepository, you need to create a custom interface
     * that custom interface will be extended by this order repository, and implemented by a concrete class implementation
     * however this is not enough, because when injecting the custom interface in the controller it will see 2 beans
     * the orderimpl and the order repo because they are both related to orterrepocustom
     * so the orderimpl needs to be annotated with @Primary so it knows
     * that for the filed orderRepoCustom in the controller, the orderimpl should be used
     */

    List<TacoOrder> findByDeliveryZip(String deliveryZip);
    List<TacoOrder> readOrdersByDeliveryZipAndPlacedAtBetween(String deliveryZip, LocalDateTime startDate, LocalDateTime endDate);

}
