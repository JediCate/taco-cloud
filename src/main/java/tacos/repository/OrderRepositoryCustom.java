package tacos.repository;

import org.springframework.stereotype.Repository;
import tacos.domain.TacoOrder;
@Repository()
public interface OrderRepositoryCustom<S> {

    <S extends TacoOrder> S save(S entity);
}
