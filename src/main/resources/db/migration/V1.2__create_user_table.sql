  create table if not exists taco_user (
      id serial PRIMARY KEY,
      username varchar(50) not null,
      password varchar(255) not null,
      fullname varchar(50) not null,
      street varchar(16) not null,
      city varchar(16) not null,
      state varchar(2) not null,
      zip varchar(6) not null,
      phone_number varchar(15) not null,
      provider varchar(15)
  );