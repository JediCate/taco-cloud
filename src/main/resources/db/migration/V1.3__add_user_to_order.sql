BEGIN;
ALTER TABLE taco_order ADD COLUMN taco_user bigint;
UPDATE taco_order SET taco_user = 1;    -- Just a silly example
ALTER TABLE taco_order ALTER COLUMN taco_user SET NOT NULL;
COMMIT;